<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhraseTranslation extends Model
{
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = ['body'];
}
