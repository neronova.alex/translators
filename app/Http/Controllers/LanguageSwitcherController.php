<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageSwitcherController extends Controller
{
    /**
     * @param Request $request
     * @param string $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switcher(Request $request, string $locale)
    {
        $request->session()->put('locale', $locale);
        return back();
    }
}
