<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhraseRequest;
use App\Models\Phrase;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhrasesController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $phrases = Phrase::all();
        return view('phrases.index', compact('phrases'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('phrases.create');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PhraseRequest $request)
    {
        $phrase = new Phrase();

        $user = Auth::user();
        $data = [ 'user_id' => $user->id,
                    'ru' =>
                [
                    'body' => $request->input('body'),
                ]
        ];

        $phrase->create($data);

        return redirect(route('home'));
    }


    /**
     * @param Phrase $phrase
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Phrase $phrase)
    {
        return view('phrases.show', compact('phrase'));
    }


    /**
     * @param Request $request
     * @param Phrase $phrase
     * @return RedirectResponse
     */
    public function update(Request $request, Phrase $phrase): RedirectResponse
    {
        $data = [];
        if(!is_null($request->input('en_body'))) {
            $data['en'] = ['body' => $request->input('en_body')];
        }

        if(!is_null($request->input('es_body'))) {
            $data['es'] = ['body' => $request->input('es_body')];
        }

        if(!is_null($request->input('fr_body'))) {
            $data['fr'] = ['body' => $request->input('fr_body')];
        }

        if(!is_null($request->input('de_body'))) {
            $data['de'] = ['body' => $request->input('de_body')];
        }

        if(!is_null($data)) {
            $phrase->update($data);
            return redirect(route('home'));
        }

    }
}
