<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->session()->exists('locale')) {
            $request->session()->put('locale', config('app.locale', 'ru'));
        }
        app()->setLocale($request->session()->get('locale'));
        return $next($request);
    }
}
