<?php

use App\Http\Controllers\PhrasesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function () {
    Route::get('/', [PhrasesController::class, 'index'])->name('home');

    Route::resource('phrases', PhrasesController::class)->only(['index', 'create', 'store', 'show', 'update']);

    Auth::routes();

});

Route::get('language/{locale}', [App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')->where('locale', 'en|ru');
