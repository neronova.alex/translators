<?php

namespace Database\Factories;

use App\Models\Phrase;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhraseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Phrase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        $es_faker = \Faker\Factory::create('es_ES');
        $fr_faker = \Faker\Factory::create('fr_FR');
        $de_faker = \Faker\Factory::create('de_DE');
        return [
            'ru' => [
                'body' => 'RU - ' . $ru_faker->sentence(5)
            ],
            'en' => [
                'body' => $this->faker->sentence(5)
            ],
            'es' => [
                'body' => 'ES - ' . $es_faker->sentence(5),
            ],
            'fr' => [
                'body' => 'FR - ' . $fr_faker->sentence(5),
            ],
            'de' => [
                'body' => 'DE - ' . $de_faker->sentence(5),
            ],
        ];
    }
}
