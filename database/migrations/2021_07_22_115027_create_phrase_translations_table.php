<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhraseTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrase_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('phrase_id')->constrained()->cascadeOnDelete();
            $table->string('locale')->index();
            $table->string('body');
            $table->unique(['phrase_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phrase_translations');
    }
}
