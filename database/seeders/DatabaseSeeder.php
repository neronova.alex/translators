<?php

namespace Database\Seeders;

use App\Models\Phrase;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(5)
            ->has(Phrase::factory()->count(3))
            ->create();
    }
}
