<?php

return [
    'welcome' =>'Welcome!',
    'phrases' => 'Phrases',
    'create new phrase' => 'Create new phrase',
    'back' => 'Back',
    'login' => 'Login',
    'register' => 'Register',
    'name' => 'Name',
    'e-mail address' => 'E-mail Address',
    'password' => 'Password',
    'confirm password' => 'Confirm password',
    'remember me' => 'Remember Me',
    'forgot your password' => 'Forgot your passport',
    'reset password' => 'Reset password',
    'send password reset link' => 'Send Password Reset Link',
    'verify your email address' => 'Verify Your Email Address',
    'required' => 'please fill out this field',
    'submit' => 'Save',
    'logout' => 'Logout'
];
