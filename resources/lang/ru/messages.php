<?php

return [
    'welcome' =>'Добро пожаловать!',
    'phrases' => 'Фразы',
    'create new phrase' => 'Добавьте новую фразу',
    'back' => 'Назад',
    'login' => 'Логин',
    'register' => 'Регистрация',
    'name' => 'Имя',
    'e-mail address' => 'Адрес электронной почты',
    'password' => 'Пароль',
    'confirm password' => 'Подтверждение пароля',
    'remember me' => 'Запомнить',
    'forgot your password' => 'Забыли пароль',
    'reset password' => 'Восстановить пароль',
    'send password reset link' => 'Отправить ссылку для сброса пароля',
    'verify your email address' => 'Подтвердите ваш адрес электронной почты',
    'required' => 'Поле обязательно для заполнения',
    'submit' => 'Сохранить',
    'logout' => 'Выйти'
];
