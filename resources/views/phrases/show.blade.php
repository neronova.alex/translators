@extends('layouts.app')
@section('content')

    <div class="container">
        <form enctype="multipart/form-data" method="post" action="{{ route('phrases.update', ['phrase' => $phrase]) }}">
            @method('put')
            @csrf
        <div>
            @if (Auth::check())
                <div class="form-group">
                    <label for="ru_body"><b>Русский</b></label>
                    <input type="text" class="form-control" id="ru_body" name="ru_body" value="{{$phrase->translate('ru')->body}}">
                </div>
                <div class="form-group">
                    <label for="en_body"><b>English</b></label>
                    <input type="text" class="form-control" id="en_body" name="en_body"
                           @if(!is_null($phrase->translate('en'))) value="{{$phrase->translate('en')->body}}"@endif>
                </div>
                <div class="form-group">
                    <label for="es_body"><b>Español</b></label>
                    <input type="text" class="form-control" id="es_body" name="es_body"
                           @if(!is_null($phrase->translate('es'))) value="{{$phrase->translate('es')->body}}"@endif>
                </div>
                <div class="form-group">
                    <label for="fr_body"><b>Français</b></label>
                    <input type="text" class="form-control" id="fr_body" name="fr_body"
                           @if(!is_null($phrase->translate('fr'))) value="{{$phrase->translate('fr')->body}}"@endif>
                </div>
                <div class="form-group">
                    <label for="de_body"><b>Deutsche</b></label>
                    <input type="text" class="form-control" id="de_body" name="de_body"
                           @if(!is_null($phrase->translate('de'))) value="{{$phrase->translate('de')->body}}"@endif>
                </div>
                <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>

    @else
                <ul>
                <li>
                    <h5>{{$phrase->translate('ru')->body}}</h5>
                </li>
            </ul>
            <ul>
                <li>
                    <h5>{{$phrase->translate('en')->body}}</h5>
                </li>
            </ul>
            <ul>
                <li>
                    <h5>{{$phrase->translate('es')->body}}</h5>
                </li>
            </ul>
            <ul>
                <li>
                    <h5>{{$phrase->translate('fr')->body}}</h5>
                </li>
            </ul>
            <ul>
                <li>
                    <h5>{{$phrase->translate('de')->body}}</h5>
                </li>
            </ul>
        </div>

            <div>
                <a href="{{route('phrases.index')}}" class="btn btn-primary btn-sm"><b>@lang('messages.back')</b></a>
            </div>

    @endif

    </div>
@endsection
