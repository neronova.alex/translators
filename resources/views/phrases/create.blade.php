@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col main pt-5 mt-3">
            <h1>@lang('messages.create new phrase')</h1>
            <form method="post" action="{{ route('phrases.store') }}">
                @csrf
                <div class="form-group">
                    <label for="body"><b>Русский</b></label>
                    <input type="text" class="form-control" id="body" name="body">
                </div>
                <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
            </form>
        </div>
    </div>

@endsection
