@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <h3>
                    @lang('messages.phrases')
                </h3>
            </div>
        </div>
        <div>
            @csrf

                @foreach($phrases as $phrase)
                    <div>
                        <ul>
                            <li>
                                <h5><a href="{{route('phrases.show', ['phrase' => $phrase])}}">{{$phrase->translate('ru')->body}}</a></h5>
                            </li>
                        </ul>
                    </div>
                @endforeach

        </div>

        <div class="row">
            <div class="col">
                @if (Auth::check())
                    <a href="{{route('phrases.create')}}" class="btn btn-primary btn-sm
              active" role="button" aria-pressed="true">@lang('messages.create new phrase')</a>
                @endif
            </div>
        </div>

    </div>
@endsection


